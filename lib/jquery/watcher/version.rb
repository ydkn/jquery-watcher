# frozen_string_literal: true

module Jquery
  module Watcher
    VERSION = '0.3.1'
  end
end
