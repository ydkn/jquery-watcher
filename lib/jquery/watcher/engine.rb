# frozen_string_literal: true

module Jquery
  module Watcher
    # Rails engine to register jquery-watcher with rails
    class Engine < ::Rails::Engine
      initializer 'jquery-watcher.assets.precompile' do |app|
        app.config.assets.paths << root.join('assets', 'javascripts').to_s
      end
    end
  end
end
