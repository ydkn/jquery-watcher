# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jquery/watcher/version'

Gem::Specification.new do |spec|
  spec.name        = 'jquery-watcher'
  spec.version     = Jquery::Watcher::VERSION
  spec.authors     = ['Florian Schwab']
  spec.email       = ['me@ydkn.de']
  spec.summary     = %(Allow initialization of javascript code regardless of document.ready, turbolinks or dom changes
                      (e.g. Ajax).)
  spec.description = %(Allow initialization of javascript code regardless of document.ready, turbolinks or dom changes
                      (e.g. Ajax).)
  spec.homepage    = 'https://gitlab.com/ydkn/jquery-watcher'
  spec.license     = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'coffee-rails', '>= 4.0'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'
end
