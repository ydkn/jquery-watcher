# Change Log

## 0.3.1 (2017-05-28)
### Changes
- ensure callback with jQuery object

## 0.3.0 (2017-02-03)
### Changes
- Prefilter callback elements

## 0.2.0 (2017-01-31)
### Changes
- Refactored to a more future-proof api

## 0.1.0 (2017-01-30) (Initial Release)
